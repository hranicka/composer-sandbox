# Hranicka Composer Sandbox

This is a sandbox which helps you create a new composer package.

It includes:

* .gitignore rules
* composer.json
* shippable.yml for CI
* PHPUnit bootstrap


## New project creation

`$ composer create-project hranicka/composer-sandbox my-app`

Repository name should be lower case, with hyphens instead of spaces.


## Tests

See [shippable.yml](/shippable.yml) and PHPUnit config.
